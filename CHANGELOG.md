<a name="0.0.1"></a>
## 0.0.1 (2017-01-26)


### Bug Fixes

* add icon mixin to .hasicon class ([77321e9](https://bitbucket.org/rr_kodix/vw_d5_components/commits/77321e9))
* add webpack-dev-server ([5dfc0f1](https://bitbucket.org/rr_kodix/vw_d5_components/commits/5dfc0f1))
* added eslint to dep for terminal functionality ([23a8a2d](https://bitbucket.org/rr_kodix/vw_d5_components/commits/23a8a2d))
* box-sizing and $em-base for rem function ([d5c8e7b](https://bitbucket.org/rr_kodix/vw_d5_components/commits/d5c8e7b))
* disable fixed icon size by default ([2c0dfea](https://bitbucket.org/rr_kodix/vw_d5_components/commits/2c0dfea))
* fix strange pug_mixin.input issue ([6a4c77f](https://bitbucket.org/rr_kodix/vw_d5_components/commits/6a4c77f))
* fixed strange error pug_mixins.input not a function ([d7d29ae](https://bitbucket.org/rr_kodix/vw_d5_components/commits/d7d29ae))
* fonts vwtextoffice cyrillic added ([e9f7f1f](https://bitbucket.org/rr_kodix/vw_d5_components/commits/e9f7f1f))
* masterpage real fix ([6975318](https://bitbucket.org/rr_kodix/vw_d5_components/commits/6975318))
* private repo ssh address ([775c042](https://bitbucket.org/rr_kodix/vw_d5_components/commits/775c042))
* update grid ([02f9f3a](https://bitbucket.org/rr_kodix/vw_d5_components/commits/02f9f3a))
* webpack dev server hot module reload fix for style and js ([3a8b1c3](https://bitbucket.org/rr_kodix/vw_d5_components/commits/3a8b1c3))


### Features

* add .pug extension to include ([dd85399](https://bitbucket.org/rr_kodix/vw_d5_components/commits/dd85399))
* add icofonts from icomoon ([42c4760](https://bitbucket.org/rr_kodix/vw_d5_components/commits/42c4760))
* add icon to input mixin ([2ab4c3e](https://bitbucket.org/rr_kodix/vw_d5_components/commits/2ab4c3e))
* add maim navigation js ([ade63e0](https://bitbucket.org/rr_kodix/vw_d5_components/commits/ade63e0))
* add mobile logo ([fba6dd6](https://bitbucket.org/rr_kodix/vw_d5_components/commits/fba6dd6))
* add scroll lock class to body ([10cc597](https://bitbucket.org/rr_kodix/vw_d5_components/commits/10cc597))
* add search form ([23c5ada](https://bitbucket.org/rr_kodix/vw_d5_components/commits/23c5ada))
* add search form to submenu ([031e720](https://bitbucket.org/rr_kodix/vw_d5_components/commits/031e720))
* add z-indexes and transition-timing-functions ([40780a3](https://bitbucket.org/rr_kodix/vw_d5_components/commits/40780a3))
* auto load page list in kdx nav ([555134f](https://bitbucket.org/rr_kodix/vw_d5_components/commits/555134f))
* gulp compile iconfont ([56f6335](https://bitbucket.org/rr_kodix/vw_d5_components/commits/56f6335))
* implemented new webpack bs ([2a9dc16](https://bitbucket.org/rr_kodix/vw_d5_components/commits/2a9dc16))
* initial dealer commit ([384a0a8](https://bitbucket.org/rr_kodix/vw_d5_components/commits/384a0a8))
* media function added ([28c212f](https://bitbucket.org/rr_kodix/vw_d5_components/commits/28c212f))
* minor improvements in ghost grid ([3610117](https://bitbucket.org/rr_kodix/vw_d5_components/commits/3610117))
* px to rem ([71c20b8](https://bitbucket.org/rr_kodix/vw_d5_components/commits/71c20b8))
* set fz for html ([7906e9d](https://bitbucket.org/rr_kodix/vw_d5_components/commits/7906e9d))
* z-index layers ([1a38cb0](https://bitbucket.org/rr_kodix/vw_d5_components/commits/1a38cb0))



