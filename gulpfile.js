// Main gulp components
var fs = require('fs'),
  path = require('path'),
  argv = require('yargs').argv,
  gulp = require('gulp'),
  merge2 = require('merge2'),
  $ = require('gulp-load-plugins')();

// Utility components
// var livereload = require('gulp-livereload'),
//     runSequence = require('run-sequence');

// Compile modernizr script
// gulp.task('modernizr', function () {
//   return gulp.src('./frontend/build/assets/css/**/*.css')
//     .pipe($.modernizr(
//       {
//         cache: false,
//         "options": [
//           "html5shiv",
//           "setClasses"
//         ],
//         "crawl": true,
//         "excludeTests": ['hidden']
//       }
//     ))
//     // .pipe($.print())
//     .pipe($.uglify())
//     .pipe(gulp.dest('./frontend/build/assets/js/'));
// });


gulp.task('iconfont', function () {
  var stream = merge2();

  var pathToFonts = path.join(__dirname, 'src', 'globals', 'icons');
  if (fs.existsSync(pathToFonts)) {
    var fontDirs = getDirectories(pathToFonts);
    for (var k = 0; k < fontDirs.length; k++) {
      stream.add(
        gulp.src([path.relative(__dirname, fontDirs[k].path).replace('\\', '/') + '/*.svg'])
          .pipe($.iconfontCss({
            fontName: fontDirs[k].name,
            path: 'src/devdep/gulp/_icons.scss',
            targetPath: './stylesheet.scss',
            cssClass: fontDirs[k].name,
            fontPath: './',
          }))
          .pipe($.iconfont({
            fontName: fontDirs[k].name,
            formats: ['ttf', 'eot', 'woff', 'woff2'],
            normalize: true,
          }))
          .pipe(gulp.dest(path.join(__dirname, 'src', 'globals', 'fonts', fontDirs[k].name)))
      )
    }
  }

  return stream;

  function getDirectories(srcpath) {
    return fs.readdirSync(srcpath).filter(function (file) {
      return fs.statSync(path.join(srcpath, file)).isDirectory() && fs.readdirSync(path.join(srcpath, file)).length > 0;
    }).map(function (file) {
      return {name: 'icon-'+file, path: path.join(srcpath, file)}
    });
  }
});
