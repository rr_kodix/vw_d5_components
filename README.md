# Сборщик проектов KDX
###Установка:
```bash
npm i
```
или
```bash
yarn
```

затем



```
node ./node_modules/kdx-builder/index.js 
```

Во время установки будут созданы файл конфигурации и скрипты для `npm`
###Конфиг сборщика
Настройки для сборщика находятся в файле kdx.config.json, структура:

kdx.config.json [default]:
```js
{
  "default": { // Настройки по умолчанию
    "paths": { // Пути для сборки
      "sourceRoot": "src/",
      "pages": "src/pages/",
      "output": "build/",
      "publicPath": "/",
      "assets": {
        "js": "js/",
        "css": "css/",
        "fonts": "fonts/",
        "images": "images/",
        "templates": ""
      },
      "resolve": {
        // Тут можно задать алиасы для часто используемых файлов и директорий
        //"someAlias": "./src/some/path/to/some/nested/folder" // Далее, где необхоимо, вызываем алиас вместо пути к файлу @import "someAlias";
      }
    },
    "kdxTools": false, // Меню, сетка и прочие фичи, полезные при разработке
    "devServer": { 
      "active": false, // По-умолчанию сервер не стартует
      "port": 8080 // Порт сервера, который можно переопределить ниже
    },
    "minify": false, // Минификация
    "provide": {
      // Добавление переменной в js
      //"name": "Homer" // Далее в любом js файле используем  console.log(name); 
    }
  },
  "development": { // Задача по умолчанию, запуск через npm run start
    "kdxTools": true, // Включаем dev-меню, сетку
    "devServer": {
      "active": true // Запускаем сервер
    }
  },
  "test": { // Задача, переопределяющая настройки по умолчанию, запуск через переменную NODE_ENV=test
    "paths": {
      "publicPath": "/"
    },
    "kdxTools": true
  },
  "production": { // Задача, переопределяющая настройки по умолчанию, запуск через переменную NODE_ENV=production
    "paths": {
      "assets": {
        "templates": "templates/"
      }
    },
    "minify": true
  }
  // Можно создать кастомные задачи с нужными параметрами, запуск по имени узла в json, вызов также как и с задачами выше, через переменную NODE_ENV=your_task
}
```


### Скрипты

```js
    "test": "npm run lint",
    "lint": "./node_modules/eslint --cache --ext .jsx,.js --format=node_modules/eslint-formatter-pretty .",
    "build": "SET NODE_ENV=test & node ./kdx.builder.js",
    "start": "node ./kdx.builder.js",
    "deploy": "SET NODE_ENV=production & node ./kdx.builder.js",
    "deploy-windows": "SET NODE_ENV=production & webpack --production",
    "validate": "npm ls",
    "changelog": "conventional-changelog -p angular -i CHANGELOG.md -s -r 1",
    "debug": "node webpack.config.js --debug",
    "build-unix": "NODE_ENV=test node ./kdx.builder.js",
    "deploy-unix": "NODE_ENV=production node ./kdx.builder.js",
    "build:watch": "SET NODE_ENV=test & node ./kdx.builder.js --watch",
    "build-unix:watch": "NODE_ENV=test node ./kdx.builder.js --watch",
    "deploy:watch": "SET NODE_ENV=production & node ./kdx.builder.js --watch",
    "deploy-unix:watch": "NODE_ENV=production node ./kdx.builder.js --watch",
    "update": "node ./kdx.builder.js --update"
```
