// Utility
var argv = require('yargs').argv;
var util = require('util');

if (argv.debug)
  console.log(util.inspect(require('kdx-builder')(), false, null));
else
  module.exports = require('kdx-builder')((void 0), {
    publicPath: '/_auto/d5/'
  });

