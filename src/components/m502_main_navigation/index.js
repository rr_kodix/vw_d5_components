import $ from 'jquery';
import _ from 'lodash';
import Media from 'globals/js/media.js';
import toRem from 'globals/js/utility/to-rem';
import * as util from 'globals/js/utility/core';

import VWNav from './MainNavComponent/Nav';

const Navigation = new VWNav($('.js_header_menu'), $('.js_m502_submenu'));

if (process.env.NODE_ENV === 'development') {
  util.devlog(Navigation);
}


// const $mob_menu_btn = $('.js_m502_mob_menu_switcher');
// const $menu_btn = $('.js_m502_menu_switcher');
// const $hidden_menu = $('.js_m502_submenu');
//
// function switchMainMenu() {
//   $mob_menu_btn.toggle();
//   $hidden_menu.toggleClass('open');
//   if (Media.isMobile) {
//     $('body').toggleClass('scroll-lock');
//   }
// }
//
// $mob_menu_btn.click(() => {
//   switchMainMenu();
// });
//
// $menu_btn.click(() => {
//   switchMainMenu();
// });
//
//
// Menu hide on scroll bottom
(function () {
  let $menu = $('.js_header_menu');
  let $window = $(window);

  function scrollControlThrottled() {
    let lst = 0;

    function scrollControl() {
      var st = $(this).scrollTop();
      if (st < lst || st < 150) {
        $menu.removeClass('slide-up');
      } else {
        $menu.addClass('slide-up');
      }
      lst = st;
    }

    return _.throttle(scrollControl, 400);
  }

  $window.on('resize load', function () {
    if ($window.width() < Media.breakpoints.tablet) {
      $window.on('scroll.hmh', scrollControlThrottled());
    } else {
      $window.off('scroll.hmh');
    }
  });

}());
//
//
// // Menu items same size and its flow
// (function () {
//   let $navLinks = $('.vw_m502_btn');
//   let $section = $('.js_m520_main_sections');
//   let $logo = $section.children().first();
//
//   let $sublayerSection = $('.js_m520_sublayer_sections');
//
//
//   function setMenuItemsHeight($items) {
//     let maxElementHeight = $(_.maxBy($items, (item) => $(item).outerHeight())).outerHeight();
//     $items.css('height', toRem(maxElementHeight) + 'rem');
//     return maxElementHeight;
//   }
//
//   class MenuItem {
//     constructor($item) {
//       this.$main = $item;
//       this.icon = $item.find('.hasicon').attr('class').replace('hasicon ', '');
//       this.link = $item.attr('href');
//       this.label = $item.find('.vw_m502_link_label').text();
//
//       // Flags
//       this.mainVisible = true;
//
//       this.createSublayerItem();
//     }
//
//     createSublayerItem() {
//       this.$sublayerItem = $(`
//         <li class="js_vw_m502_nav_item_flow">
//           <a class="row" href="${this.link}" target="_self">
//             <div class="gridcontainer">
//               <div class="grid_2 grid_sm_3 vw_m502_icon_wrapper">
//                 <div class="hasicon ${this.icon}"></div>
//               </div>
//               <div class="grid_10 grid_sm_9 vw_m502_link_label"><span class="vw_m502_link_label_text">${this.label}</span></div>
//             </div>
//           </a>
//         </li>
//       `);
//       $sublayerSection.append(this.$sublayerItem);
//     }
//
//     flow() {
//       if(!this.mainVisible) {
//         return;
//       }
//       this.$main.addClass('js_vw_m502_nav_item_flow');
//       this.$sublayerItem.removeClass('js_vw_m502_nav_item_flow');
//       this.mainVisible = false;
//     }
//
//     reset() {
//       if(this.mainVisible) {
//         return;
//       }
//       this.$main.removeClass('js_vw_m502_nav_item_flow');
//       this.$sublayerItem.addClass('js_vw_m502_nav_item_flow');
//       this.mainVisible = true;
//     }
//   }
//
//
//   let linkHeight = setMenuItemsHeight($navLinks);
//   let menuItemsMap = _.map($navLinks.slice(0, -1), (link) => {
//       return new MenuItem($(link));
//     }
//   );
//
//
//   console.log(menuItemsMap);
//
//
//   function menuRender() {
//     if ($(window).width < Media.breakpoints.tablet) {
//       return;
//     }
//     let linkSectionHeight = $section.height() - $logo.height() - linkHeight;
//
//     _.map(menuItemsMap, (item) => item.reset());
//
//     // for(let i = linkSectionHeight, k = 0; i >= 0 && k < 5)
//
//   }
//
//
//   document.addEventListener('DOMContentLoaded', menuRender);
//   window.addEventListener('resize', menuRender);
// }());
//
//
//
//
//
//
