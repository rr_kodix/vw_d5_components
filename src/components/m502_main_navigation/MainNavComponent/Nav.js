/**
 * Created by Kodix on 01.02.2017.
 */
import * as util from 'globals/js/utility/core';
import Media from 'globals/js/media.js';
import toRem from 'globals/js/utility/to-rem';
import _ from 'lodash';
import $ from 'jquery';


class VWNavItem {
  constructor(item, $sublayer) {
    this.$item = $(item);

    // Parse item
    this.icon = this.$item.find('.hasicon').attr('class').replace('hasicon ', '');
    this.link = this.$item.children().attr('href');
    this.label = this.$item.find('.vw_m502_link_label').text();

    this.$sublayerItem = $(`
      <li class="js_vw_m502_nav_item_flow">
        <a class="row" href="${this.link}" target="_self">
          <div class="gridcontainer">
            <div class="grid_2 grid_sm_3 vw_m502_icon_wrapper">
              <div class="hasicon ${this.icon}"></div>
            </div>
            <div class="grid_10 grid_sm_9 vw_m502_link_label"><span class="vw_m502_link_label_text">${this.label}</span></div>
          </div>
        </a>
      </li>
    `);
    $sublayer.append(this.$sublayerItem);

    this.mainVisible = true;
  }


  flow() {
    if (!this.mainVisible) {
      return;
    }
    this.$item.addClass('js_vw_m502_nav_item_flow');
    this.$sublayerItem.removeClass('js_vw_m502_nav_item_flow');
    this.mainVisible = false;
  }

  reset() {
    if (this.mainVisible) {
      return;
    }
    this.$item.removeClass('js_vw_m502_nav_item_flow');
    this.$sublayerItem.addClass('js_vw_m502_nav_item_flow');
    this.mainVisible = true;
  }
}

/**
 * @this VWNav
 */
function collectItems() {
  return _.map(this.$main.children('ul').children().slice(1), (item) => new VWNavItem(item, this.$sublayer.children('.js_m520_sublayer_sections')));
}

export default class VWNav {

  constructor($mainSection, $sublayerSection) {
    const self = this;

    this.$main = $mainSection;
    this.$sublayer = $sublayerSection;
    this.$moreButton = $(`
      <li>
        <a class="vw_m502_btn" href="#">
          <div class="vw_m502_btn__inner">
            <div class="vw_m502_icon_wrapper">
              <div class="hasicon icon_icon-navigation-more">
                <!-- icon must be here-->
              </div>
            </div>
            <span class="vw_m502_link_label">Ещё</span>
          </div>
        </a>
      </li>
    `);
    this.NavItems = collectItems.call(this);
    if (process.env.NODE_ENV === 'development') {
      util.assert(_.isArray(this.NavItems));
      //console.log(this.NavItems);
    }


    // Assign events
    if (this.NavItems.length > 5) {
      this.$main.children('ul').append(this.$moreButton);
      this.$moreButton.on('click', () => self.openSublayer());
      for (let i = 5; i < this.NavItems.length; i++) {
        this.NavItems[i].flow();
      }
    }

    this.$mobileMoreButton = this.$main.find('.js_m502_mob_menu_switcher');
    this.$mobileMoreButton.on('click', () => self.openSublayer());

    // Set nav same size
    document.addEventListener('DOMContentLoaded', () => {
      self.setNavItemHeight();
      self.logoHeight = self.$main.children('ul').children().slice(0, 1).outerHeight();

      self.flowItems();
      if (process.env.NODE_ENV === 'development') {
        //console.log(self.logoHeight);
      }
    });

    window.addEventListener('resize', () => {
      self.flowItems();
    });
  }

  openSublayer() {
    //console.log('open sublayer');
    this.$mobileMoreButton.toggle();
    this.$sublayer.toggleClass('open');
    if (Media.isMobile) {
      $('body').toggleClass('scroll-lock');
    }
  }

  setNavItemHeight() {
    const self = this;

    this.itemHeight = $(_.maxBy(this.NavItems, (item) =>
      item.$item.outerHeight()).$item).outerHeight();

    if (process.env.NODE_ENV === 'development') {
      //console.log($(_.maxBy(this.NavItems, (item) => item.$item.outerHeight()).$item).height());
    }

    _.each(this.NavItems, (item) => item.$item.css('height', `${toRem(self.itemHeight)}rem`));
  }

  flowItems() {
    if ($(window).width() < Media.breakpoints.tablet) {
      return;
    }

    let mainSectionHeight = this.$main.outerHeight() - this.logoHeight;
    //console.log(mainSectionHeight);
    this.reset();
    for (let arrLength = this.NavItems.length, i = arrLength > 5 ? 5 : arrLength - 1; i > -1 && (i + (arrLength > 5 ? 2 : 1)) * this.itemHeight > mainSectionHeight; i--) {
      this.NavItems[i].flow();
      if (i === 0) {
        //console.log(i, this.$moreButton.find('.hasicon'));

        this.$moreButton.addClass('js_more_collapsed');
        this.$moreButton.find('.vw_m502_link_label').text('Меню');
      }
    }
  }

  reset() {
    for (let i = 0, arrLength = this.NavItems.length; i < (arrLength > 5 ? 5 : arrLength); i++) {
      this.NavItems[i].reset();
    }
    this.$moreButton.removeClass('js_more_collapsed');
    this.$moreButton.find('.vw_m502_link_label').text('Еще');
  }
}
