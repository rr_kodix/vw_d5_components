/**
 * Created by Kodix on 12.01.2017.
 */

import $ from 'jquery';

$.extend($.easing, {
  easeInOutQuint: function (x, t, b, c, d) {
    if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
    return c/2*((t-=2)*t*t*t*t + 2) + b;
  }
});

$('.js_go_to_top').on('click', function () {
  $('html, body').animate({
    scrollTop: 0
  }, 300,
  'easeInOutQuint');
});

$('.js_other_pages_select').on('change', function () {
  if(this.value != '') {
    window.location.href = this.value;
  }
});

