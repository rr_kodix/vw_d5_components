import $ from 'jquery'
import * as Media from 'globals/js/media.js';

function updateMainstageHeight() {
  var wH  = $(window).height(),
      wW  = $(window).width(),
      $stage = $('.js_vw_m301_stage_img_container'),
      stageH = wH * .8,
      minStageH = 480;

  if(wW < Media.tablet) {
    $stage.height(($stage.width() * 9) / 16);
  } else if(wW >= Media.tablet && wW < Media.laptop) {
    $stage.height(minStageH);
  } else {
    $stage.height(stageH < minStageH ? minStageH : stageH);
  }
}

$(window).resize(updateMainstageHeight);
$(document).ready(updateMainstageHeight);
