// m502_main_navigation js
require('components/m502_main_navigation/index.js');

// variables, mixins and global styles for whole project
require('./style/style_global.scss');

// Global js functionality
require('./js/script_global.js');
require('./js/typography.js');
require('components/m520_footer/index.js');
