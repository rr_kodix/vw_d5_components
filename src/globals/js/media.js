import $ from 'jquery';
import _ from 'lodash';

class Media {
  constructor() {
    const self = this;
    this.breakpoints = {
      phone: 320,
      fablet: 480,
      tablet: 768,
      laptop: 1024,
      desktop: 1281,
      widescreen: 1486,
    };
    this.device = '';
    this.isMobile = true;

    this.updateDeviceClass();

    $(window).resize(() =>
      self.updateDeviceClass()
    );
    $(document).ready(() =>
      self.updateDeviceClass()
    );
  }

  updateDeviceClass() {
    let wW = $(window).width();
    var self = this;
    var sortedBreakpoints = (_.map(this.breakpoints, (v, key) => [key, v])).sort((a, b) => a[1] - b[1]);
    let oldDevice = self.device;
    self.isMobile = wW < this.breakpoints.tablet;

    _.each(sortedBreakpoints, (v) => {
      if (wW > v[1]) {
        self.device = v[0];
      }
    });

    // fallback to 'phone' on sub-phone widths
    self.device = self.device ? self.device : _.first(sortedBreakpoints)[0];

    if(!!oldDevice && oldDevice != self.device) {
      self.emitDeviceChange();
    }
  }

  emitDeviceChange() {
    const self = this;
    const event = new CustomEvent('deviceChanged', {
      detail: {
        device: self.device
      }
    });
    document.dispatchEvent(event);
  }
}

const MediaSingleton = (function () {
  let instance;

  function createInstance() {
    var object = new Media();
    return object;
  }

  return {
    getInstance() {
      if (!instance) {
        instance = createInstance();
      }
      return instance;
    }
  };
})();


export default MediaSingleton.getInstance();
