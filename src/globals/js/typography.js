import $ from 'jquery';
//require('bigtext/dist/bigtext');
require("imports-loader?jQuery=jquery!bigtext/dist/bigtext.js");
import Media from 'globals/js/media.js';

function responsiveTypo() {
  var sizes = {
    head: {
      mobile:   [20, 46],
      fablet:   [30, 74],
      tablet:   [40, 96],
      laptop:   [60, 140],
      desktop:  [76, 186],
      widescreen: [86, 216]
    },
    subhead: {
      mobile:   14,
      fablet:   22,
      tablet:   28,
      laptop:   40,
      desktop:  52,
      widescreen: 60
    }
  };


  $('.js_responsive_header').bigtext(1, {
    minFontSize: sizes.head[Media.device[0]],
    maxFontSize: sizes.head[Media.device[1]]
  });
  $('.js_responsive_subheader').bigtext(1, {
    minFontSize: sizes.subhead[Media.device[0]],
    maxFontSize: sizes.subhead[Media.device[0]]
  });
}

$(document).ready(responsiveTypo);

$(document).on('deviceChanged', (e) => {
  console.log(e.detail.device);
});

$(window).resize(responsiveTypo);
